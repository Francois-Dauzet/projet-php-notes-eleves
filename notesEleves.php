<?php

declare(strict_types=1);

// Variables
(bool) $finSaisie = true;
(int) $nombreEleve = 0;
(string) $topNomHistoire = "";
(string) $topPrenomHistoire = "";
(float) $topNoteHistoire = -1.0;
(string) $badNomPhysique = "";
(string) $badPrenomPhysique = "";
(float) $badNotePhysique = 20;

// Tableau de stockage
(array) $infoEleves = [];

// Boucle de saisie
while ($finSaisie){

    (array) $NotesEleve = [
        "nom" => "",
        "prenom" => "",
        "noteMath" => 0.0,
        "notePhysique" => 0.0,
        "noteFrancais" => 0.0,
        "noteHistoire" => 0.0,
        "noteAnglais" => 0.0,
        "moyenne" => 0.0
    ];

    // Variables 
    (string) $saisieNom = "";
    (string) $saisiePrenom = "";
    (float) $saisieNoteMath = -1.0;
    (float) $SaisieNotePhysique = -1.0;
    (float) $SaisieNoteFrancais = -1.0;
    (float) $saisieNoteHistoire = -1.0;
    (float) $SaisieNoteAnglais = -1.0;
    (float) $totalNote = 0.0;
    (float) $moyenne = 0.0;
    (string) $choixFin = "";
    (float) $additionTotaleMath = 0.0;
    (float) $moyenneTotaleMath = 0.0;
    (float) $moyenneTotaleMath = 0.0;
    (float) $additionTotaleAnglais = 0.0;
    (float) $moyenneTotaleAnglais = 0.0;
    (float) $moyenneTotaleAnglais = 0.0;

    // Demande et stockage du Nom
    while(!$saisieNom):
        print("Nom de l'élève ? ");
        $saisieNom = trim(strval(fgets(STDIN)));
    endwhile;

    // Demande et stockage du Prenom
    while(!$saisiePrenom):
        print("Prénom de l'élève ? ");
        $saisiePrenom = trim(strval(fgets(STDIN)));
    endwhile;

    // Demande et stockage de la Note de math
    while($saisieNoteMath < 0 || $saisieNoteMath > 20):
        print("Note en math ? ");
        $saisieNoteMath = floatval(trim(fgets(STDIN)));
    endwhile;

    // Demande et stockage de la Note de physique
    while($SaisieNotePhysique < 0 || $SaisieNotePhysique > 20):
        print("Note en physique ? ");
        $SaisieNotePhysique = floatval(trim(fgets(STDIN)));
    endwhile;

    // Calcul de la note la plus basse en physique
    if($SaisieNotePhysique < $badNotePhysique){
        $badNomPhysique = $saisieNom;
        $badPrenomPhysique = $saisiePrenom;
        $badNotePhysique = $SaisieNotePhysique;
    }

    // Demande et stockage de la Note de français
    while($SaisieNoteFrancais < 0 || $SaisieNoteFrancais > 20):
        print("Note en français ? ");
        $SaisieNoteFrancais = floatval(trim(fgets(STDIN)));
    endwhile;

    // Demande et stockage de la Note de histoire
    while($saisieNoteHistoire < 0 || $saisieNoteHistoire > 20):
        print("Note en histoire ? ");
        $saisieNoteHistoire = floatval(trim(fgets(STDIN)));
    endwhile;

    // Calcul meilleur note histoire
    if($saisieNoteHistoire > $topNoteHistoire){
        $topNomHistoire = $saisieNom;
        $topPrenomHistoire = $saisiePrenom;
        $topNoteHistoire = $saisieNoteHistoire;
    }

    // Demande et stockage de la Note de anglais
    while($SaisieNoteAnglais < 0 || $SaisieNoteAnglais > 20):
        print("Note en anglais ? ");
        $SaisieNoteAnglais = floatval(trim(fgets(STDIN)));
    endwhile;

    // Moyenne éléve pour chaques éléves
    $totalNote = $totalNote + $saisieNoteMath;
    $totalNote = $totalNote + $SaisieNotePhysique;
    $totalNote = $totalNote + $SaisieNoteFrancais;
    $totalNote = $totalNote + $saisieNoteHistoire;
    $totalNote = $totalNote + $SaisieNoteAnglais;

    $moyenne = $totalNote / 5;

    // Envoie des saisies dans le tableau $notesEleve
    $notesEleve['nom'] = $saisieNom;
    $notesEleve['prenom'] = $saisiePrenom;
    $notesEleve['noteMath'] = $saisieNoteMath;
    $notesEleve['notePhysique'] = $SaisieNotePhysique;
    $notesEleve['noteFrancais'] = $SaisieNoteFrancais;
    $notesEleve['noteHistoire'] = $saisieNoteHistoire;
    $notesEleve['noteAnglais'] = $SaisieNoteAnglais;
    $notesEleve['moyenne'] = $moyenne;

    // Envoie des saisies dans le tableau $infoEleves
    $infoEleves[] = $notesEleve;

    // Calcul du nombre d'élèves saisies
    $nombreEleve++;

    // Demande et stockage de la réponse oui ou non pour la boucle de fin de saisie
    while ( $choixFin != 'non' && $choixFin != 'oui'){
        print(PHP_EOL . 'Avez vous d\'autres élèves a saisir (oui / non) ?' . PHP_EOL);
        $choixFin = trim(strval(fgets(STDIN)));
    }

    // Défini sur true si "oui" est saisie
    $finSaisie = ($choixFin == 'oui');
}

// Récapitulatif des élèves saisies
print(PHP_EOL . 'Récapitulatif des saisies effectuer :' . PHP_EOL);
print_r($infoEleves);

// Affichage de la moyenne générale
printf(PHP_EOL . 'La moyenne de la classe est de %g/20' . PHP_EOL,
    $moyenne
);

// Calcul de la moyenne de la classe en math 
(array) $moyenneMath = array_column($infoEleves, 'noteMath');

foreach($moyenneMath as & $additionTotaleMath){
    $moyenneTotaleMath = $additionTotaleMath + $moyenneTotaleMath;
}

$moyenneTotaleMath = $moyenneTotaleMath / $nombreEleve;

printf(PHP_EOL . 'La moyenne générale de la classe en mathématique est de %g/20' . PHP_EOL,
    $moyenneTotaleMath
);

// Calcul de la moyenne de la classe en anglais 
(array) $moyenneAnglais = array_column($infoEleves, 'noteAnglais');

foreach($moyenneAnglais as & $additionTotaleAnglais){
    $moyenneTotaleAnglais = $additionTotaleAnglais + $moyenneTotaleAnglais;
}

$moyenneTotaleAnglais = $moyenneTotaleAnglais / $nombreEleve;

// Print final
printf(PHP_EOL . 'La moyenne générale de la classe en anglais est de %g/20' . PHP_EOL,
    $moyenneTotaleAnglais
);

// Nom et prénom du meilleurs de la classe en histoire
printf(PHP_EOL . 'La meilleur note en histoire est attribuer a %s %s avec une note de %d/20 ' . PHP_EOL,
    $topNomHistoire,
    $topPrenomHistoire,
    $topNoteHistoire
);

// Nom et prénom du plus mauvais de la classe en physique
printf(PHP_EOL . 'La note la plus basse en physique est attribuer a %s %s avec une note de %d/20 ' . PHP_EOL,
    $badNomPhysique,
    $badPrenomPhysique,
    $badNotePhysique
);